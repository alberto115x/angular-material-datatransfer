import { Component, Inject, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DatatransferFacade } from '../facades/datatransfer.facade';
import { IDatatransferItem } from '../models/datatransfer-item.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'amd-edit-dialog',
    templateUrl: 'edit-dialog.component.html'
})

export class EditDialogComponent implements AfterViewInit {

    datatransferFacade: DatatransferFacade;
    mode: string;
    item: IDatatransferItem;
    itemPath: string;
    itemName: string;
    itemDescription: string;
    errorMessage: string;
    editFormControl: FormControl;

    constructor(
        public dialogRef: MatDialogRef<EditDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.datatransferFacade = (this.data.datatransferFacade as DatatransferFacade);
        this.mode = this.data.mode;
        this.item = (this.data.item as IDatatransferItem);
        this.itemPath = this.item.path;
        this.itemName = this.item.name;
        this.itemDescription = this.item.description;
        this.editFormControl = new FormControl('', []);

    }

    ngAfterViewInit() {

    }

    close(): void {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.close();
    }

    editPath(): void {
        try {
            this.datatransferFacade.editPath(this.item, this.item.description, this.itemDescription);
            this.close();
        } catch (error) {
            this.errorMessage = error;
        }
    }

    editFilename(): void {
        try {
            this.datatransferFacade.editFilename(this.item, this.itemName);
            this.close();
        } catch (error) {
            this.errorMessage = error;
        }
    }
  editDescription(): void {
    try {
      this.datatransferFacade.editDescription(this.item, this.item.description, this.itemDescription);
      this.close();
    } catch (error) {
      this.errorMessage = error;
    }
  }
}
